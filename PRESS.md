# Press
A collection of content mentioning Impression from various writers, content creators, etc.

- [Crea tus live USB con Impression - THE_CHEI$](https://thecheis.com/2023/06/07/crea-live-usb-impression/)  
  *Translated from Spanish:*  
  > No frills, no extra options, no pretensions: just open, select the image, plug in the USB and burn. As stupidly simple as effective.

- [Impression: A brand new GNOME app for flashing ISO images - Reddit](https://www.reddit.com/r/gnome/comments/1426x9g/impression_a_brand_new_gnome_app_for_flashing_iso/)
  > Wow, great to have a GTK4 app for this!
  
  > GTK4, Libadwaita, written in Rust… Wow, this is very cool! Thanks for working on this! It's time to ditch Popsicle :)

- [Cassidy James on Mastodon](https://mastodon.blaede.family/@cassidy/110500772741983943)
  > YES, THANK YOU. I have wanted this for quite some time. Etcher is great for cross-platform-ness but I don't love using an AppImage. The Raspberry Pi flasher works fine for non-RasPi stuff, but feels awkward. Popsicle is weirdly stuck in a sort of early GNOME 3 feel and never really got the polish I would have liked. Simple, straightforward, GTK4, Libadwaita… 😍
